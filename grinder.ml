open Td;;

(* iterate over the grid and make a list of empty spaces (valid moves) *)
let valid_moves grid =
  let result = ref [] in
    Array.iteri (fun ir row -> Array.iteri (fun ic t ->
      if (t = `Empty) then 
        result := (ir, ic) :: !result
    ) row) grid;
  !result
;;

(* pick a random item from a list *)
let random_choice l =
  let num = List.length l in
  let choice = Random.int num in
    List.nth l choice
;;

(* get the list of valid moves and return a random one *)
let engine state = 
  let valid = valid_moves state.grid in
    Io.issue_order (random_choice valid)
;;

