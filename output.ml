module Td = struct
  type tile = [ `Friend | `Enemy | `Empty ]
  
  type dir = [ `N | `E | `S | `W | `NE | `SE | `NW | `SW ];;
  
  type state =
   {
    grid : tile array array;
    mutable row_count : int;
    mutable id : char;
   }
  ;;
  

end;;

module Debug = struct
  let debug s = output_string stderr s; flush stderr;;
  
  let debug_char c = output_char stderr c; flush stderr;;

end;;

module Io = struct
  open Td;;
  
  let height = 29;;
  let width = 29;;
  
  (* read height + 1 lines from stdin, return the list of strings *)
  let read_lines () =
    let line_count = ref 0 in
    let rec read_loop acc =
      let line = read_line () in
      line_count := !line_count + 1;
      if !line_count = height + 1 then
       (
        List.rev (line :: acc)
       )
      else
        read_loop (line :: acc)
    in
    try Some (read_loop []) with End_of_file -> None
  ;;
  
  (* tokenizer from rosetta code *)
  let split_char sep str =
    let string_index_from i =
      try Some (String.index_from str i sep)
      with Not_found -> None
    in
    let rec aux i acc = match string_index_from i with
      | Some i' ->
          let w = String.sub str i (i' - i) in
          aux (succ i') (w::acc)
      | None ->
          let w = String.sub str i (String.length str - i) in
          List.rev (w::acc)
    in
    aux 0 []
  ;;
  
  (* messy stateful tracking of row count during map parsing *)
  let next_grid_row state =
    let result = state.grid.(state.row_count) in
      state.row_count <- state.row_count + 1;
      result
  ;;
  
  (* parse a line of map data into the grid *)
  let add_map_line state l =
    let grid_row = next_grid_row state in
    let count = ref 0 in
    let add_tile c = 
      let tile =
        if c = state.id then `Friend
        else if c = '-' then `Empty
        else `Enemy
      in
        grid_row.(!count) <- tile;
        count := !count + 1
    in
      String.iter add_tile l
  ;;
  
  (* parse an input string consisting of a single token *)
  let one_token state t =
    match String.length t with
    | 1 -> state.id <- t.[0]
    | 29 -> add_map_line state t (* 29 = width FIXME *)
    | _ -> ()
  ;;
  
  (* parse a line of input *)
  let parse_line state l =
    let tokens = split_char ' ' l in
    match List.length tokens with
    | 1 -> one_token state (List.nth tokens 0)
    | _ -> ()
  ;;
  
  (* If read_lines in update returns None, it means that either the engine did 
   * not send as much info as was expected (not likely unless the format changes)
   * or that this program has processed the game state, submitted its orders,
   * and then returned for more input (more likely). Since hackerrank bots are
   * run afresh every turn and should exit cleanly, "exit 0" is the correct
   * response to None here.
   *)
  let update state =
    match read_lines () with
    | None -> exit 0
    | Some ll ->
        List.iter (fun l -> parse_line state l) ll
  ;;
  
  let issue_order (row, col) =
    Printf.printf "%d %d" row col
  ;;

end;;

module Grinder = struct
  open Td;;
  
  (* iterate over the grid and make a list of empty spaces (valid moves) *)
  let valid_moves grid =
    let result = ref [] in
      Array.iteri (fun ir row -> Array.iteri (fun ic t ->
        if (t = `Empty) then 
          result := (ir, ic) :: !result
      ) row) grid;
    !result
  ;;
  
  (* pick a random item from a list *)
  let random_choice l =
    let num = List.length l in
    let choice = Random.int num in
      List.nth l choice
  ;;
  
  (* get the list of valid moves and return a random one *)
  let engine state = 
    let valid = valid_moves state.grid in
      Io.issue_order (random_choice valid)
  ;;
  

end;;

module Gol = struct
  open Td;;
  
  (* height and width are specified as constants in Io since they are not 
   * given as inputs*)
  let new_state () =
   {
    grid = Array.make_matrix Io.height Io.width `Empty;
    row_count = 0;
    id = '!';
   }
  ;;
  
  (* main input -> engine -> ouput loop (loop part not needed for hackerrank) *)
  let loop engine =
    let rec take_turn i state =
      Io.update state;
      begin try
       (
        engine state;
        flush stdout;
       )
      with exc ->
       (
        Debug.debug (Printf.sprintf
           "Exception in turn %d :\n" i);
        Debug.debug (Printexc.to_string exc);
        raise exc
       )
      end;
      take_turn (i + 1) state
    in
      (* Random.self_init () *) (* seed RNG if you want *)
      let proto_state = new_state () in
      take_turn 0 proto_state
  ;;
  
  loop Grinder.engine

end;;

