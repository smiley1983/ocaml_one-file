This program opens a list of .ml files, and writes their content, enclosed in "module Filename struct ... end;;" lines, to a single output.ml file.

You can then compile the single file, after giving it a proper entry point (removing "module ... end" from around the toplevel file), provided that the list of modules has been provided in a sensible order with respect to hierarchy of use.

You currently have to provide the list of modules in the source, just look at the end of one_file.ml and it should be clear.

To build everything:

ocamlbuild gol.native 		# check that the source works

ocamlbuild one_file.native	# build the one_file program

./one_file.native		# invoke it

ocamlbuild output.native	# check that you can build the result

Now if you submit the output.ml file to hackerrank, then if everything has gone correctly, it will compile.
